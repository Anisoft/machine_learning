import joblib
import numpy as np

# 1. A l'aide de la fonction load de joblib, charger le fichier et stocker le résultat dans la variable modele
modele = joblib.load("regression.joblib")

# 2. Afficher le type de la variable et vérifier qu'il s'agit bien d'une instance de la class LinearRegression
print(type(modele))

# 3. Créer un tableau numpy 2D avec une seule ligne ayant  les valeurs 110, 3, 0 et stocker dans la variable X
X = np.array([[110, 3, 0]])

# 4. Avec la méthode predict de la classe model, calculer le prix de la maison prédite et stocker le résultat dans la variable “predicted_price"
predicted_price = modele.predict(X)
print(predicted_price)