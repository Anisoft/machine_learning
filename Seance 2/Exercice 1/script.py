import pandas as pd 
from sklearn.linear_model import LinearRegression
import joblib 
from sklearn.model_selection import train_test_split

#data loading
df = pd.read_csv('houses.csv')

#split dataset in train and test dataset
train, test = train_test_split(df)

X_train = train[['size', 'nb_rooms', 'garden']]
y_train = train['price']

model = LinearRegression()
model.fit(X_train, y_train)

joblib.dump(model, "regression.joblib")