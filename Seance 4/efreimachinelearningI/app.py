# CODE DU SERVEUR
from flask import Flask, request
import json

app = Flask(__name__)

def load_model():

    import joblib
    with open('regression_maison.joblib', 'rb') as f:
        model = joblib.load(f)
    return model


#model = load_model()

@app.route("/", methods=['POST'])
def hello_world():
   
    print(request.json)

    #y = model.predict(X_data)
    data = {
        'y': [1, 2, 3] #y.tolist()
    }
    return json.dumps(data) #conversion de dictionnaire en json


if __name__ == "__main__":

    app.run()